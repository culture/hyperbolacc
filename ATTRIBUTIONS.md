__HyperbolaCC Attributions:__

* ___[Crazytoon (José Silva) &lt;crazytoon@hyperbola.info&gt;][CRAZYTOON]___

>     Original author for drawings, most clean-ups and some colours (2015-2017)
>     Hyper Bola (hypercolour) H emblem and cape colours (2017)
>     Bola (parapale and paraviolet) colours (2016)
>     GNU (gnublue) colours (2015)

* ___[coadde (Márcio Silva) &lt;coadde@hyperbola.info&gt;][COADDE]___

>     Digitalizations, clean-ups and colouring.
>     Hyper Bola (hypercolour) most colours (2017)
>     Hyper Bola (hypersilver and hypergray) colours (2017)
>     Bola (hypercolour, hypersilver, hypergray) colours (2017)
>     Bola (precolour, paracolour [precolour enhanced],
>           protocolour and parapink [paraviolet enhanced]) colours (2015-2017)
>     GNU (gnublue) eyes colours (2015-2017)

* ___[Emulatorman (André Silva) &lt;emulatorman@hyperbola.info&gt;][EMULATORMAN]___

>     Hyperbola, Hyperbola GNU/Linux-libre and HyperbolaBSD wordmarks (2017-2019)
>     Hyperbola (solid silver) abstrack logotype and Hyperbola (light and dark) wordmark colours (2017)
>     Hyper Bola (hypercolour and hypersilver) some colours suggestion (2017)
>     Bola (hypercolour and hypersilver) some colours suggestion (2017)
[CRAZYTOON]:crazytoon@hyperbola.info "Crazytoon (José Silva) <crazytoon@hyperbola.info>"
[COADDE]:coadde@hyperbola.info "coadde (Márcio Silva) <coadde@hyperbola.info>"
[EMULATORMAN]:emulatorman@hyperbola.info "Emulatorman (André Silva) <emulatorman@hyperbola.info>"
