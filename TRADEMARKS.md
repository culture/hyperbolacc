__List of trademarks:__

*   __[Hyperbola&trade;][HTM]__
    is a trademark of _[Hyperbola&trade; Project][HYPERBOLA]_.
*   __[HyperbolaBSD&trade;][HBSD]__
    is a trademark of _[Hyperbola&trade; Project][HYPERBOLA]_.

__List of registered trademarks used in
Hyperbola&trade; GNU&reg;/Linux&reg;-libre logotype:__

*   __[GNU&reg;][GNU]__ and
    *[Free Software Foundation&reg;][FSF_LRT] [(FSF&reg;)][FSF_SRT]*<br/>
    is a registered trademark of _[Free Software Foundation&reg;][FSF]_.
*   __[Linux&reg;][LINUX]__
    is a registered trademark of _[Linus Torvalds][LMI]_.


[HTM]: https://www.hyperbola.info/
    "Hyperbola™ trademark"
[HBSD]: https://www.hyperbola.info/
    "HyperbolaBSD™ trademark"
[HYPERBOLA]: https://www.hyperbola.info/
    "Hyperbola™ Project"

[GNU]: https://tarr.uspto.gov/servlet/tarr?regser=serial&amp;entry=85380218
    "GNU® trademark"
[FSF_LRT]: https://tarr.uspto.gov/servlet/tarr?regser=serial&amp;entry=87909237
    "Free Software Foundation® trademark"
[FSF_SRT]: https://tarr.uspto.gov/servlet/tarr?regser=serial&amp;entry=87909272
    "FSF® trademark"
[FSF]: https://www.fsf.org/
    "Free Software Foundation®"

[LINUX]: https://tarr.uspto.gov/servlet/tarr?regser=serial&amp;entry=74560867
    "Linux® trademark"
[LMI]: https://www.linuxmark.org/
    "Linux Mark Institute"
