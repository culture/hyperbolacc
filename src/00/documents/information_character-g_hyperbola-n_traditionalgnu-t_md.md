<!--
  -- [Hyperbola™] Traditional GNU🄯 character information 2020/03/26
  --
  -- ---------------------------------------------------------------------------
  --
  -- The attributions are:
  -- * [author] (2020/03/26) Márcio Silva <coadde@hyperbola.info>.
  --
  -- ---------------------------------------------------------------------------
  --
  -- You can redistribute it and/or modify it under the terms of either:
  --
  -- a) The Free Art License
  --    as published by Copyleft Attitude;
  --    either version 1.3, or (at your option) any later version
  --    (read "https://artlibre.org/licence/lal/en/").
  --
  -- b) The Creative Commons® Attribution-ShareAlike 4.0 International License
  --    as published by Creative Commons®;
  --    either version 4.0, or (at your option) any later version
  --    (read "https://creativecommons.org/licenses/by-sa/4.0/").
  --
  -- c) The GNU® Free Documentation License
  --    as published by the Free Software Foundation®;
  --    either version 1.3, or (at your option) any later version;
  --    with no Invariant Sections,
  --    no Front-Cover Texts, and no Back-Cover Texts;
  --    with the following clarifications and special exceptions:
  --
  --      * As a first special exception, the copyright holders
  --        of this document does not permit you to add
  --        any Invariant Sections, Front-Cover Texts and/or Back-Cover Texts.
  --
  --      * As a second special exception, the copyright holders
  --        of this document does not permit you to
  --        make a combined document which contains
  --        any Invariant Sections, Front-Cover Texts and/or Back-Cover Texts.
  --
  --      * These exceptions do not however invalidate any other reasons why the
  --        document might be covered by the GNU® Free Documentation License.
  --
  --      * If you modify this document, combine any documents and/or
  --        update this license, you must retain these exceptions statement
  --        to your version of the document.
  --
  --    (read "https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/
  --     licenses/fdl_plus_se.txt").
  -->


Character Information:
======================

### Drawing style:
*   __Cartoon.__

### Animation style:
*   __Tex Avery.__

### Character creation date:
*   __2016/07/28__ <br/>
    *(is a derivation of "A Bold GNU Head"<br/>
    that was created by "Aur&eacute;lio A. Heckert" in 2003<br/>
    and "A GNU Head" that was created by "Etienne Suvasa" in 1996)*__.__

### Fictional serie:
*   __Hyperbola&trade;.__

### Name:
*   __Traditional GNU__ *(default; english)*__.__
*   __Traditional GNU__ *(others languages)*__.__

### Age:
*   3 years.

### Specie:
*   Gnu.

### Colours:
*   Gnuretro.

### Sex:
*   Male.

### Gender:
*   Male.

### Sexual orientation:
*   Heterosexual.

### Personality:
*   Free.
*   Mocker.
*   Perseveres.
*   Serious.

### Actions:
*   Mock the losers, idiots or nofree.

### Dislikes:
*   Androids.
*   Apples.
*   Blobbed Penguins.
    *   Tux _(penguin)_.
*   Daemons.
    *   Beastie _(daemon)_.
*   Dragonflies.
*   Idiots.
*   Losers.
*   Nonfree Animals.
    *   Blinky _(fish)_.
    *   Hexley _(platypus)_.
    *   Puffy _(pufferfish)_.
*   Orange Flags.
*   Proprietary Dogs.
*   Proprietary Lions.
*   Proprietary Orcas.
*   Trolls.
*   Windows _(plural of window)_.

### Likes:
*   Clean Penguins.
    *   Freedo _(penguin)_.
*   Free Animals.
    *   Amarok _(wolf)_.
    *   Amanda _(panda)_.
    *   Dragora _(dragon or mandrake?)_.
    *   elePHPant.
    *   Kate _(woodpecker)_.
    *   Katie _(dragon)_.
    *   Kiki _(cyber squirrel)_.
    *   Konqi _(dragon)_.
    *   Sara _(human)_.
    *   Slonik _(elephant)_.
    *   Suzanne _(monkey)_.
    *   Ututo _(lizard)_.
    *   Wilber _(coyote)_.
    *   Xue _(mouse)_.
*   Genius.
*   Hedgehogs _(from Hedgewars)_.
*   Liberty Cats.
    *   Bola _(cat from Hyperbola)_.
    *   Hyper Bola _(hyper hero cat)_.
    *   Para Bola _(cat from Parabola)_.
*   Liberty Gnus.
    *   GNU _(character)_.
*   Lightweight Mouses.
*   Replicant Androids.
    *   Replicant _(android)_.
*   Tees _(from Teeworlds)_.
*   Winners.

Licenses:
---------

*   [__Traditional GNU&#127279;__ character information _2020/03/26_][FILE]<br/>
    by _[M&aacute;rcio Silva][COADDE]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.


[FILE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/documents/information_character-g_hyperbola-n_traditionalgnu-t_md.md
    "Traditional GNU🄯 character information"

[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"

[FAL]: https://artlibre.org/licence/lal/en/
    "Free Art License version 1.3"
[CCBYSA]: https://creativecommons.org/licenses/by-sa/4.0/
    "Creative Commons® Attribution-ShareAlike 4.0 International License"
[FDL+SE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/licenses/fdl_plus_se.txt
    "GNU® Free Documentation License version 1.3 with the special exceptions"
