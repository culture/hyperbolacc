__HyperbolaCC&#127279; 2014-2020 (The Hyperbola&trade; Contents Collection)__
=============================================================================

The _trademarks_ are included in:

*   The __[TRADEMARKS.md][TRADEMARKS]__ file which contains
    _all trademarks_ for this _source_.
*   _Inside_ each _file_.

The _attributions_ are included in:

*   The __[ATTRIBUTIONS.md][ATTRIBUTIONS]__ file which contains
    _all attributions_ for this _source_.
*   _Inside_ each _file_.

---

The content files: in
_[src/*/animations][ANIMATIONS]_,
_[src/*/backgrounds][BACKGROUNDS]_,
_[src/*/comics][COMICS]_,<br/>
_[src/*/documents][DOCUMENTS]_,
_[src/*/drawings][DRAWINGS]_,
_[src/*/icons][ICONS]_,
_[src/*/logotypes][LOGOTYPES]_,<br/>
_[src/*/models][MODELS]_,
_[src/*/scenes][SCENES]_,
_[src/*/textures][TEXTURES]_ and
_[src/*/wallpapers][WALLPAPERS]_;<br/>
you can _redistribute it_ and/or _modify it_ under the _terms of either_:

1.  The ___[Free Art License][FAL]___<br/>
    as published by ___Copyleft Attitude___;<br/>
    either _version_ ___1.3___, or _(at your option)_ any _later_ version<br/>
    _(read_ ***[COPYING_FAL_V1_3][FAL]***_)_.

2.  The
    ___[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]___<br/>
    as published by ___[Creative Commons&reg;][CC]___;<br/>
    either _version_ ___4.0___, or _(at your option)_ any _later_ version<br/>
    _(read_ ***[COPYING_CCBYSA_V4_0][CCBYSA]***_)_.

3.  The ___[GNU&reg; Free Documentation License][FDL+SE]___<br/>
    as published by the ___[Free Software Foundation&reg;][FSF]___;<br/>
    either _version_ ___1.3___, or _(at your option)_ any _later_ version;<br/>
    with _no Invariant Sections_,<br/>
    _no Front-Cover Texts_, and _no Back-Cover Texts_;<br/>
    with the following _clarifications_ and _special exceptions_:

    *   As a ___first special exception___, the copyright holders<br/>
        of this document _does not permit_ you to ___add___<br/>
        any _Invariant Sections_, _Front-Cover Texts_ and/or _Back-Cover Texts_.

    *   As a ___second special exception___, the copyright holders<br/>
        of this document _does not permit_ you to<br/>
        ___make a combined document___ which contains<br/>
        any _Invariant Sections_, _Front-Cover Texts_ and/or _Back-Cover Texts_.

    *   These _exceptions_ do ___not___ however_invalidate_ any other reasons
        why the<br/>
        _document_ might be _covered by_ the
        ___GNU&reg; Free Documentation License___.

    *   If you _modify this document_, _combine any documents_ and/or<br/>
        _update this license_, you must ___retain___
        these _exceptions statement_<br/>
        to your _version of the document_.<br/>

    _(read_ ***[COPYING_FDL_P_SE_V1_3][FDL+SE]***_)_.

---

The content files: in
_[src/*/sprite_sheets][SPRITE_SHEETS]_ and
_[src/*/tile_sheets][TILE_SHEETS]_;<br/>
you can _redistribute it_ and/or _modify it_ under the _terms of either_:

1.  The ___[Free Art License][FAL]___<br/>
    as published by ___Copyleft Attitude___;<br/>
    either _version_ ___1.3___, or _(at your option)_ any _later_ version<br/>
    _(read_ ***[COPYING_FAL_V1_3][FAL]***_)_.

2.  The
    ___[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]___<br/>
    as published by ___[Creative Commons&reg;][CC]___;<br/>
    either _version_ ___4.0___, or _(at your option)_ any _later_ version<br/>
    _(read_ ***[COPYING_CCBYSA_V4_0][CCBYSA]***_)_.

3.  The ___[GNU&reg; Free Documentation License][FDL+SE]___<br/>
    as published by the ___[Free Software Foundation&reg;][FSF]___;<br/>
    either _version_ ___1.3___, or _(at your option)_ any _later_ version;<br/>
    with _no Invariant Sections_,<br/>
    _no Front-Cover Texts_, and _no Back-Cover Texts_;<br/>
    with the following _clarifications_ and _special exceptions_:

    *   As a ___first special exception___, the copyright holders<br/>
        of this document _does not permit_ you to ___add___<br/>
        any _Invariant Sections_, _Front-Cover Texts_ and/or _Back-Cover Texts_.

    *   As a ___second special exception___, the copyright holders<br/>
        of this document _does not permit_ you to<br/>
        ___make a combined document___ which contains<br/>
        any _Invariant Sections_, _Front-Cover Texts_ and/or _Back-Cover Texts_.

    *   These _exceptions_ do ___not___ however_invalidate_ any other reasons
        why the<br/>
        _document_ might be _covered by_ the
        ___GNU&reg; Free Documentation License___.

    *   If you _modify this document_, _combine any documents_ and/or<br/>
        _update this license_, you must ___retain___
        these _exceptions statement_<br/>
        to your _version of the document_.<br/>

    _(read_ ***[COPYING_FDL_P_SE_V1_3][FDL+SE]***_)_.

4.  The ___[GNU&reg; General Public License][GPL]___<br/>
    as published by the ___[Free Software Foundation&reg;][FSF]___;<br/>
    either _version_ ___3.0___, or _(at your option)_ any _later_ version<br/>
    _(read_ ***[COPYING_GPL_V3_0][GPL]***_)_.

---

The content files: in
_[src/*/palettes][PALETTES]_;<br/>
you can _redistribute it_ and/or _modify it_ under the _terms of either_:

1.  The ___[CC0 1.0 Universal][CC0]____<br/>
    as published by ___[Creative Commons&reg;][CC]___;<br/>
    _(read_ ***[COPYING_CC0][CC0]***_)_.


[CC]: https://creativecommons.org/
    "Creative Commons®"
[FSF]: https://www.fsf.org/
    "Free Software Foundation®"

[TRADEMARKS]: TRADEMARKS.md
    "HyperbolaCC🄯 Trademarks"
[ATTRIBUTIONS]: ATTRIBUTIONS.md
    "HyperbolaCC🄯 Attributions"

[ANIMATIONS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/animations
    "HyperbolaCC🄯 animations"
[BACKGROUNDS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/backgrounds
    "HyperbolaCC🄯 backgrounds"
[COMICS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/comics
    "HyperbolaCC🄯 comics"
[DOCUMENTS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/documents
    "HyperbolaCC🄯 documents"
[DRAWINGS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/drawings
    "HyperbolaCC🄯 drawings"
[ICONS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/icons
    "HyperbolaCC🄯 icons"
[LOGOTYPES]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/logotypes
    "HyperbolaCC🄯 logotypes"
[MODELS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/models
    "HyperbolaCC🄯 models"
[PALETTES]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/palettes
    "HyperbolaCC🄯 palettes"
[SCENES]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/scenes
    "HyperbolaCC🄯 scenes"
[SPRITE_SHEETS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/sprite_sheets
    "HyperbolaCC🄯 sprite sheets"
[TEXTURES]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/textures
    "HyperbolaCC🄯 textures"
[TILE_SHEETS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/tile_sheets
    "HyperbolaCC🄯 tile sheets"
[WALLPAPERS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/wallpapers
    "HyperbolaCC🄯 wallpapers"

[FAL]: https://artlibre.org/licence/lal/en/
    "Free Art License version 1.3"
[CCBYSA]: https://creativecommons.org/licenses/by-sa/4.0/
    "Creative Commons® Attribution-ShareAlike 4.0 International License"
[FDL+SE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/COPYING_FDL_P_SE_V1_3
    "GNU® Free Documentation License version 1.3 with the special exceptions"
[GPL]: https://www.gnu.org/licenses/gpl-3.0.html
    "GNU® General Public License version 3.0"
[CC0]: https://creativecommons.org/publicdomain/zero/1.0/
    "CC0 1.0 Universal"
