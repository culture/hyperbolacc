<!--
  -- [Hyperbola™] Mary Daisy🄯 character information 2020/03/26
  --
  -- ---------------------------------------------------------------------------
  --
  -- The attributions are:
  -- * [author] (2020/03/26) Márcio Silva <coadde@hyperbola.info>.
  --
  -- ---------------------------------------------------------------------------
  --
  -- You can redistribute it and/or modify it under the terms of either:
  --
  -- a) The Free Art License
  --    as published by Copyleft Attitude;
  --    either version 1.3, or (at your option) any later version
  --    (read "https://artlibre.org/licence/lal/en/").
  --
  -- b) The Creative Commons® Attribution-ShareAlike 4.0 International License
  --    as published by Creative Commons®;
  --    either version 4.0, or (at your option) any later version
  --    (read "https://creativecommons.org/licenses/by-sa/4.0/").
  --
  -- c) The GNU® Free Documentation License
  --    as published by the Free Software Foundation®;
  --    either version 1.3, or (at your option) any later version;
  --    with no Invariant Sections,
  --    no Front-Cover Texts, and no Back-Cover Texts;
  --    with the following clarifications and special exceptions:
  --
  --      * As a first special exception, the copyright holders
  --        of this document does not permit you to add
  --        any Invariant Sections, Front-Cover Texts and/or Back-Cover Texts.
  --
  --      * As a second special exception, the copyright holders
  --        of this document does not permit you to
  --        make a combined document which contains
  --        any Invariant Sections, Front-Cover Texts and/or Back-Cover Texts.
  --
  --      * These exceptions do not however invalidate any other reasons why the
  --        document might be covered by the GNU® Free Documentation License.
  --
  --      * If you modify this document, combine any documents and/or
  --        update this license, you must retain these exceptions statement
  --        to your version of the document.
  --
  --    (read "https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/
  --     licenses/fdl_plus_se.txt").
  -->


Character Information:
======================

### Drawing style:
*   __Hybrid:__ Traditional drawing and Cartoon**.**

### Animation style:
*   __Hybrid:__ Tex Avery, Traditional animation and Rotoscoping**.**

### Character creation date:
*   __2016/06/03.__

### Fictional serie:
*   __Hyperbola&trade;.__

### Name:
*   __Mary Daisy__ *(default; english)*__.__
*   __Mear&imacr;deij&imacr;
    &#12300;&#12513;&#12450;&#12522;&#12540;&#12487;&#12452;&#12472;&#12540;&#12301;__
    *(japanese)*__.__
*   __Mary Daisy__ *(others languages)*__.__

### Age:
*   3.000.000.000.000 years.

### Specie:
*   Werecat.

### Colours:
*   Marydaisy?.

### Sex:
*   Female.

### Gender:
*   Female.

### Sexual orientation:
*   Heterosexual.

### Personality:
*   Decisive.
*   Free.
*   Friendly.
*   Loving.
*   Perseveres.

### Dislikes:
*   Proprietary Dogs.
*   Trolls.
*   Jealous Punk Women _(humans; jealousy for Hyper Bola cat, Bola cat or Para Bola cat)_.
    *   Jealous Punk and Extroverted Woman _(human)_.
    *   Jealous Punk and Tall Woman _(human)_.
    *   Jealous Punk and Shy Woman _(human)_.
    *   Jealous Punk and Fat Woman _(human)_.

### Likes:
*   Liberty Cats.
    *   Bola _(cat)_.
    *   Hyper Bola _(hyper hero cat)_.
    *   Para Bola _(cat)_.
*   Reliable Werecats.

Licenses:
---------

*   [__Mary Daisy&#127279;__ character information _2020/03/26_][FILE]<br/>
    by _[M&aacute;rcio Silva][COADDE]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.


[FILE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/documents/information_character-g_hyperbola-n_marydaisy-t_md.md
    "Mary Daisy🄯 character information"

[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"

[FAL]: https://artlibre.org/licence/lal/en/
    "Free Art License version 1.3"
[CCBYSA]: https://creativecommons.org/licenses/by-sa/4.0/
    "Creative Commons® Attribution-ShareAlike 4.0 International License"
[FDL+SE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/licenses/fdl_plus_se.txt
    "GNU® Free Documentation License version 1.3 with the special exceptions"
