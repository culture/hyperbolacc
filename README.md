<!--
  -- Read me text 2019/01/07
  --
  -- ---------------------------------------------------------------------------
  --
  -- The attributions are:
  -- * [author] (2019/01/07) Márcio Silva <coadde@hyperbola.info>.
  --
  -- ---------------------------------------------------------------------------
  --
  -- You can redistribute it and/or modify it under the terms of either:
  --
  -- a) The Free Art License
  --    as published by Copyleft Attitude;
  --    either version 1.3, or (at your option) any later version
  --    (read "https://artlibre.org/licence/lal/en/").
  --
  -- b) The Creative Commons® Attribution-ShareAlike 4.0 International License
  --    as published by Creative Commons®;
  --    either version 4.0, or (at your option) any later version
  --    (read "https://creativecommons.org/licenses/by-sa/4.0/").
  --
  -- c) The GNU® Free Documentation License
  --    as published by the Free Software Foundation®;
  --    either version 1.3, or (at your option) any later version;
  --    with no Invariant Sections,
  --    no Front-Cover Texts, and no Back-Cover Texts;
  --    with the following clarifications and special exceptions:
  --
  --      * As a first special exception, the copyright holders
  --        of this document does not permit you to add
  --        any Invariant Sections, Front-Cover Texts and/or Back-Cover Texts.
  --
  --      * As a second special exception, the copyright holders
  --        of this document does not permit you to
  --        make a combined document which contains
  --        any Invariant Sections, Front-Cover Texts and/or Back-Cover Texts.
  --
  --      * These exceptions do not however invalidate any other reasons why the
  --        document might be covered by the GNU® Free Documentation License.
  --
  --      * If you modify this document, combine any documents and/or
  --        update this license, you must retain these exceptions statement
  --        to your version of the document.
  --
  --    (read "https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/
  --     COPYING_FDL_P_SE_V1_3").
  -->


<img alt="HYPERBOLA_LOGOTYPE_IMAGE" src="https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/logotypes/logotype_symbol_and_text-g_hyperbola-n_hyperbola_symbol_and_text-i0-u_sldstrPblr_sldshpPblr_grdshdPblr_grdlgtPblr-c_silver_and_hyperbola_dark_text-r7168x2048px-a0f1urwgothic_s0-t_svg1d1basic.svg" text="Hyperbola™ logotype image" width="512">
<img alt="HYPER_BOLA_CHARACTER_IMAGE" src="https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_hyperbola-n_hyperbola-p_standing_in_bipedal_and_leaning_his_right_hand-i1-u_sldstr_sldshp_sldshdPblr_sldlgtPblr-c_hypercolour-r2048px2-a0f0s0-t_svg1d1basic.svg" text="Hyper Bola🄯 character image" height="160">

__HyperbolaCC&#127279; 2014-2020 (The Hyperbola&trade; Contents Collection)__
=============================================================================

It's collection of _content files_ from
___[Hyperbola&trade; Project][HYPERBOLA]___.

Most _content files_ are _plain text_ data format only,<br/>
and are separeated by categories:

*   _[Comics][COMICS]_.
*   _[Drawings][DRAWINGS]_.
*   _[Documents][DOCUMENTS]_.
*   _[Icons][ICONS]_.
*   _[Logotypes][LOGOTYPES]_.
*   _[Wallpapers][WALLPAPERS]_.
*   _Others_.

---

The _license terms_ are located:

*   The __[COPYING.md][COPYING]__ file which contains
    _all license terms_ for this _source_.
*   _Inside_ of each _file_.

The _trademarks_ are included in:

*   The __[TRADEMARKS.md][TRADEMARKS]__ file which contains
    _all trademarks_ for this _source_.
*   _Inside_ each _file_.

The _attributions_ are included in:

*   The __[ATTRIBUTIONS.md][ATTRIBUTIONS]__ file which contains
    _all attributions_ for this _source_.
*   _Inside_ each _file_.

---

__Note:__

> __HyperbolaCC&#127279;__ doesn't contain _SVG DOM_ and _SVG Tiny microDOM_
> <br/>_(_***ECMAScript*** _and_ ***CSS***_)_.

License:
--------

[__Read me__ text _2019/01/07_][FILE]<br/>
by _[M&aacute;rcio Silva][COADDE]_ is under the terms of either:<br/>
_[Free Art License version 1.3][FAL]_,<br/>
_[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
_[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.

Licenses for _logotype_ and _character_:
----------------------------------------

*   [__HyperbolaBSD&trade;__ logotype image _2017/05/12_][LOGOTYPE]<br/>
    by _[Andr&eacute; Silva][EMULATORMAN]_, _[M&aacute;rcio Silva][COADDE]_ and
    _[Jos&eacute; Silva][CRAZYTOON]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.

*   [__Hyper Bola&#127279;__ character image _2017/04_, _2019/01/22_ and
    _2019/07/10_][CHARACTER]<br/>
    by _[Jos&eacute; Silva][CRAZYTOON]_, _[M&aacute;rcio Silva][COADDE]_ and
    _[Andr&eacute; Silva][EMULATORMAN]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.


[HYPERBOLA]: https://www.hyperbola.info/
    "Hyperbola™ Project"

[COPYING]: COPYING.md
    "HyperbolaCC🄯 License"
[TRADEMARKS]: TRADEMARKS.md
    "HyperbolaCC🄯 Trademarks"
[ATTRIBUTIONS]: ATTRIBUTIONS.md
    "HyperbolaCC🄯 Attributions"

[COMICS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/comics
    "HyperbolaCC🄯 comics"
[DOCUMENTS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/documents
    "HyperbolaCC🄯 documents"
[DRAWINGS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/drawings
    "HyperbolaCC🄯 drawings"
[ICONS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/icons
    "HyperbolaCC🄯 icons"
[LOGOTYPES]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/logotypes
    "HyperbolaCC🄯 logotypes"
[WALLPAPERS]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/src/00/wallpapers
    "HyperbolaCC🄯 wallpapers"

[FILE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/README.md
    "Read me text"

[LOGOTYPE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/documents/information_character-g_hyperbola-n_hyperbola-t_md.md
    "HyperbolaBSD™ logotype image"
[CHARACTER]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_hyperbola-n_hyperbola-p_standing_in_bipedal_and_leaning_his_right_hand-i1-u_sldstr_sldshp-c_hypercolour-r2048px2-a0f0s0-t_svg1d2tiny.svg
    "Hyper Bola🄯 character image"

[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"
[CRAZYTOON]: crazytoon@hyperbola.info
    "José Silva (Crazytoon) <crazytoon@hyperbola.info>"
[EMULATORMAN]: emulatorman@hyperbola.info
    "André Silva (Emulatorman) <emulatorman@hyperbola.info>"

[FAL]: https://artlibre.org/licence/lal/en/
    "Free Art License version 1.3"
[CCBYSA]: https://creativecommons.org/licenses/by-sa/4.0/
    "Creative Commons® Attribution-ShareAlike 4.0 International License"
[FDL+SE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/COPYING_FDL_P_SE_V1_3
    "GNU® Free Documentation License version 1.3 with the special exceptions"
