<!--
  -- [Rosaline™] Rosaline🄯 character information 2020/01/04
  --
  -- ---------------------------------------------------------------------------
  --
  -- The attributions are:
  -- * [author] (2020/01/04) Márcio Silva <coadde@hyperbola.info>.
  --
  -- ---------------------------------------------------------------------------
  --
  -- You can redistribute it and/or modify it under the terms of either:
  --
  -- a) The Free Art License
  --    as published by Copyleft Attitude;
  --    either version 1.3, or (at your option) any later version
  --    (read "https://artlibre.org/licence/lal/en/").
  --
  -- b) The Creative Commons® Attribution-ShareAlike 4.0 International License
  --    as published by Creative Commons®;
  --    either version 4.0, or (at your option) any later version
  --    (read "https://creativecommons.org/licenses/by-sa/4.0/").
  --
  -- c) The GNU® Free Documentation License
  --    as published by the Free Software Foundation®;
  --    either version 1.3, or (at your option) any later version;
  --    with no Invariant Sections,
  --    no Front-Cover Texts, and no Back-Cover Texts;
  --    with the following clarifications and special exceptions:
  --
  --      * As a first special exception, the copyright holders
  --        of this document does not permit you to add
  --        any Invariant Sections, Front-Cover Texts and/or Back-Cover Texts.
  --
  --      * As a second special exception, the copyright holders
  --        of this document does not permit you to
  --        make a combined document which contains
  --        any Invariant Sections, Front-Cover Texts and/or Back-Cover Texts.
  --
  --      * These exceptions do not however invalidate any other reasons why the
  --        document might be covered by the GNU® Free Documentation License.
  --
  --      * If you modify this document, combine any documents and/or
  --        update this license, you must retain these exceptions statement
  --        to your version of the document.
  --
  --    (read "https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/
  --     licenses/fdl_plus_se.txt").
  -->


<img alt="ROSALINE_CHARACTER_IMAGE" src="https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_rosaline-n_rosaline-p_standing_contains_roseline_text-i0draft-u_sldstr_noshp-c_none-r2048px2-a0f0s0-t_svg1d2tiny.svg" title="Rosaline🄯 character image" height="352">

Character Information:
======================

### Drawing style:
*   __Hybrid:__ Traditional drawing and Cartoon**.**

### Animation style:
*   __Hybrid:__ Traditional animation and Rotoscoping**.**

### Character creation date:
*   __2014.__

### Fictional serie:
*   __Rosaline&trade;.__

### Name:
*   __Rosaline__ *(default; english)*__.__
    *   __Ros__ *(english; short form)*__.__
*   __Rozarin
    &#12300;&#12525;&#12470;&#12522;&#12531;&#12301;__
    &#91;english translated: Rosalin&#93; *(japanese)*__.__
*   __Roseline__ *(french)*__.__
*   __Rosalinda__ *(italian)*__.__
*   __Rosalina__ *(portuguese and spanish)*__.__
*   __Roslindis__ *(ancient germanic)*__.__
*   __Roseline__ *(original; french)*__.__
*   __Roseline__ *(others languages)*__.__

### Age:
*   8 years.

### Specie:
*   Human.

### Sex:
*   Female.

### Gender:
*   Female.

### Sexual orientation:
*   Heterosexual.

Licenses:
---------

*   [__Rosaline&#127279;__ character image _2014_ and
    _2019/12/23_][IMAGE]<br/>
    by _[Jos&eacute; Silva][CRAZYTOON]_ and _[M&aacute;rcio Silva][COADDE]_
    is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.

*   [__Rosaline&#127279;__ character information _2020/01/04_][FILE]<br/>
    by _[M&aacute;rcio Silva][COADDE]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.


[IMAGE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_rosaline-n_rosaline-p_standing_contains_roseline_text-i0draft-u_sldstr_noshp-c_none-r2048px2-a0f0s0-t_svg1d2tiny.svg
    "Rosaline🄯 character image"
[FILE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/documents/information_character-g_rosaline-n_rosaline-t_md.md
    "Rosaline🄯 character information"

[CRAZYTOON]: crazytoon@hyperbola.info
    "José Silva (Crazytoon) <coadde@hyperbola.info>"
[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"

[FAL]: https://artlibre.org/licence/lal/en/
    "Free Art License version 1.3"
[CCBYSA]: https://creativecommons.org/licenses/by-sa/4.0/
    "Creative Commons® Attribution-ShareAlike 4.0 International License"
[FDL+SE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/licenses/fdl_plus_se.txt
    "GNU® Free Documentation License version 1.3 with the special exceptions"
