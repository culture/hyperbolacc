<!--
  -- [Rosaline™] Emil the Rat🄯 character information 2020/01/04
  --
  -- ---------------------------------------------------------------------------
  --
  -- The attributions are:
  -- * [author] (2020/01/04) Márcio Silva <coadde@hyperbola.info>.
  --
  -- ---------------------------------------------------------------------------
  --
  -- You can redistribute it and/or modify it under the terms of either:
  --
  -- a) The Free Art License
  --    as published by Copyleft Attitude;
  --    either version 1.3, or (at your option) any later version
  --    (read "https://artlibre.org/licence/lal/en/").
  --
  -- b) The Creative Commons® Attribution-ShareAlike 4.0 International License
  --    as published by Creative Commons®;
  --    either version 4.0, or (at your option) any later version
  --    (read "https://creativecommons.org/licenses/by-sa/4.0/").
  --
  -- c) The GNU® Free Documentation License
  --    as published by the Free Software Foundation®;
  --    either version 1.3, or (at your option) any later version;
  --    with no Invariant Sections,
  --    no Front-Cover Texts, and no Back-Cover Texts;
  --    with the following clarifications and special exceptions:
  --
  --      * As a first special exception, the copyright holders
  --        of this document does not permit you to add
  --        any Invariant Sections, Front-Cover Texts and/or Back-Cover Texts.
  --
  --      * As a second special exception, the copyright holders
  --        of this document does not permit you to
  --        make a combined document which contains
  --        any Invariant Sections, Front-Cover Texts and/or Back-Cover Texts.
  --
  --      * These exceptions do not however invalidate any other reasons why the
  --        document might be covered by the GNU® Free Documentation License.
  --
  --      * If you modify this document, combine any documents and/or
  --        update this license, you must retain these exceptions statement
  --        to your version of the document.
  --
  --    (read "https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/
  --     licenses/fdl_plus_se.txt").
  -->


<img alt="EMIL_CHARACTER_IMAGE" src="https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_rosaline-n_emil-p_sitting_and_looking-i0draft-u_sldstr_noshp-c_none-r2048px2-a0f0s0-t_svg1d2tiny.svg" title="Emil the Rat🄯 character image" height="352">

Character Information:
======================

### Drawing style:
*   __Cartoon.__

### Animation style:
*   __Tex Avery.__

### Character creation date:
*   __198?__ *(2014 show in Rosaline drawings)*__.__

### Fictional serie:
*   __Rosaline&trade;.__

### Name:
*   __Emil__ *(default; english, bulgarian, croatian, czech, danish,<br/>
    german, hungarian, icelandic, macedonian, norwegian, polish,<br/>
    romanian, russian, serbian, slovak, slovene and swedish)*__.__
    *   __Mille__ *(swedish; short form)*__.__
*   __Emili__ *(catalan)*__.__
*   __&Eacute;mile__ *(french)*__.__
*   __Emilios__ *(greek)*__.__
*   __Emilio__ *(italian and spanish)*__.__
*   __Em&imacr;ls__ *(latvian)*__.__
*   __Emilis__ *(lithuanian)*__.__
*   __Em&iacute;lio__ *(portuguese)*__.__
*   __Aemilius__ *(ancient roman)*__.__
*   __Emil__ *(others languages)*__.__

### Age:
*   3 months.

### Specie:
*   Rat.

### Sex:
*   Male.

### Gender:
*   Male.

### Sexual orientation:
*   Heterosexual.

Licenses:
---------

*   [__Emil the Rat&#127279;__ character image _2014_ and
    _2020/01/06_][IMAGE]<br/>
    by _[Jos&eacute; Silva][CRAZYTOON]_ and _[M&aacute;rcio Silva][COADDE]_
    is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.

*   [__Emil the Rat&#127279;__ character information _2020/01/04_][FILE]<br/>
    by _[M&aacute;rcio Silva][COADDE]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.


[IMAGE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_rosaline-n_emil-p_sitting_and_looking-i0draft-u_sldstr_noshp-c_none-r2048px2-a0f0s0-t_svg1d2tiny.svg
    "Emil the Rat🄯 character image"
[FILE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/documents/information_character-g_rosaline-n_emil_the_rat-t_md.md
    "Emil the Rat🄯 character information"

[CRAZYTOON]: crazytoon@hyperbola.info
    "José Silva (Crazytoon) <coadde@hyperbola.info>"
[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"

[FAL]: https://artlibre.org/licence/lal/en/
    "Free Art License version 1.3"
[CCBYSA]: https://creativecommons.org/licenses/by-sa/4.0/
    "Creative Commons® Attribution-ShareAlike 4.0 International License"
[FDL+SE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/licenses/fdl_plus_se.txt
    "GNU® Free Documentation License version 1.3 with the special exceptions"
