<!--
  -- [Hyperbola™] Para Bola🄯 character information 2020/03/26
  --
  -- ---------------------------------------------------------------------------
  --
  -- The attributions are:
  -- * [author] (2020/03/26) Márcio Silva <coadde@hyperbola.info>.
  --
  -- ---------------------------------------------------------------------------
  --
  -- You can redistribute it and/or modify it under the terms of either:
  --
  -- a) The Free Art License
  --    as published by Copyleft Attitude;
  --    either version 1.3, or (at your option) any later version
  --    (read "https://artlibre.org/licence/lal/en/").
  --
  -- b) The Creative Commons® Attribution-ShareAlike 4.0 International License
  --    as published by Creative Commons®;
  --    either version 4.0, or (at your option) any later version
  --    (read "https://creativecommons.org/licenses/by-sa/4.0/").
  --
  -- c) The GNU® Free Documentation License
  --    as published by the Free Software Foundation®;
  --    either version 1.3, or (at your option) any later version;
  --    with no Invariant Sections,
  --    no Front-Cover Texts, and no Back-Cover Texts;
  --    with the following clarifications and special exceptions:
  --
  --      * As a first special exception, the copyright holders
  --        of this document does not permit you to add
  --        any Invariant Sections, Front-Cover Texts and/or Back-Cover Texts.
  --
  --      * As a second special exception, the copyright holders
  --        of this document does not permit you to
  --        make a combined document which contains
  --        any Invariant Sections, Front-Cover Texts and/or Back-Cover Texts.
  --
  --      * These exceptions do not however invalidate any other reasons why the
  --        document might be covered by the GNU® Free Documentation License.
  --
  --      * If you modify this document, combine any documents and/or
  --        update this license, you must retain these exceptions statement
  --        to your version of the document.
  --
  --    (read "https://git.hyperbola.info:50100/culture/hyperbolacc.git/tree/
  --     licenses/fdl_plus_se.txt").
  -->


<img alt="PARA_BOLA_CHARACTER_IMAGE" src="https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_hyperbola-n_bola_parabola-p_standing_in_bipedal_and_leaning_his_right_hand-i1-u_sldstr_sldshp_sldshdPblr_sldlgtPblr-c_precolour-r2048px2-a0f0s0-t_svg1d1basic.svg" title="Para Bola🄯 character image" height="352">

Character Information:
======================

### Drawing style:
*   __Cartoon.__

### Animation style:
*   __Tex Avery.__

### Character creation date:
*   __2016/02/12.__

### Fictional serie:
*   __Hyperbola&trade;.__

### Name:
*   __Para Bola__ *(default; english)*__.__
    *   __Bola__ *(original; english; short form)*__.__
*   __H&#333;butsusen
    &#12300;&#25918;&#29289;&#32218;&#12301;__
    *(japanese)*__.__
*   __Para Bola__ *(others languages)*__.__

### Age:
*   3 years.

### Specie:
*   Cat _(bipedal and quadrupedal)_.

### Colours:
*   Precolour.
*   Paracolour.
*   Protocolour.
*   Parapale.
*   Parapink.
*   Paraviolet.

### Sex:
*   Male.

### Gender:
*   Male.

### Sexual orientation:
*   Heterosexual.

### Personality:
*   Confident.
*   Free.
*   Friendly.
*   Perseveres.

### Dislikes:
*   Androids.
*   Apples.
*   Blobbed Penguins.
    *   Tux _(penguin)_.
*   Cracker Monkeys.
*   Cyclist Chickens/Cocks/Hens.
*   Daemons.
    *   Beastie _(daemon)_.
*   Dragonflies.
*   Furious Cattles/Bulls/Cows.
*   Improvising Parrots.
*   Lightweight Mouses.
*   Nonfree Animals.
    *   Blinky _(fish)_.
    *   Hexley _(platypus)_.
    *   Puffy _(pufferfish)_.
*   Orange Flags.
*   Proprietary Dogs.
*   Proprietary Lions.
*   Proprietary Orcas.
*   Stability Turtles.
*   Trolls.
*   Windows _(plural of window)_.

### Likes:
*   Bloated Elephants.
*   Calm Pandas.
*   Clean Penguins.
    *   Freedo _(penguin)_.
*   Clean Sparrows.
*   Decisive Squirrels.
*   Free Animals.
    *   Amarok _(wolf)_.
    *   Amanda _(panda)_.
    *   elePHPant.
    *   Kate _(woodpecker)_.
    *   Katie _(dragon)_.
    *   Kiki _(cyber squirrel)_.
    *   Konqi _(dragon)_.
    *   Sara _(human)_.
    *   Slonik _(elephant)_.
    *   Suzanne _(monkey)_.
    *   Wilber _(coyote)_.
    *   Xue _(mouse)_.
*   Hedgehogs _(from Hedgewars)_.
*   Imprinting Ducks.
*   Liberty Cats.
*   Liberty Gnus.
    *   GNU _(character)_.
    *   Traditional GNU _(character)_.
*   Replicant Androids.
    *   Replicant _(android)_.
*   Tees _(from Teeworlds)_.
*   Reliable Werecats.
    *   Mary Daisy _(werecat)_.
*   Unstable Rabbits.

Licenses:
---------

*   [__Para Bola&#127279;__ character image _2016/02/12_ and
    _2019/01/22_][IMAGE]<br/>
    by _[Jos&eacute; Silva][CRAZYTOON]_ and<br/>
    _[M&aacute;rcio Silva][COADDE]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.

*   [__Para Bola&#127279;__ character information _2020/03/26_][FILE]<br/>
    by _[M&aacute;rcio Silva][COADDE]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.


[IMAGE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_hyperbola-n_bola_parabola-p_standing_in_bipedal_and_leaning_his_right_hand-i1-u_sldstr_sldshp_sldshdPblr_sldlgtPblr-c_paracolour-r2048px2-a0f0s0-t_svg1d1basic.svg
    "Para Bola🄯 character image"
[FILE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/documents/information_character-g_hyperbola-n_parabola-t_md.md
    "Para Bola🄯 character information"

[CRAZYTOON]: crazytoon@hyperbola.info
    "José Silva (Crazytoon) <coadde@hyperbola.info>"
[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"

[FAL]: https://artlibre.org/licence/lal/en/
    "Free Art License version 1.3"
[CCBYSA]: https://creativecommons.org/licenses/by-sa/4.0/
    "Creative Commons® Attribution-ShareAlike 4.0 International License"
[FDL+SE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/licenses/fdl_plus_se.txt
    "GNU® Free Documentation License version 1.3 with the special exceptions"
